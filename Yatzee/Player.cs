﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yatzee
{
    public class Player
    { /* The Player class contains the following fields:
        * assigned is a bool Array used to register whether a score item has been used by the player
        */

        public bool[] assigned = new bool[18];

        public Player()
        {
            Unassign();
        }

        public void Unassign()
        /* The assigned bool array is set to false.
        */        
        {
            for (int i = 0; i < 18; i++)
            {
                assigned[i] = false;
            }

            assigned[6] = true;
            assigned[7] = true;
            assigned[17] = true;
        }

    }
}
