﻿using System;

namespace Yatzee
{
    public class Player
    {
        /* The Player class contains the following fields:
        * hand is an array of Dice, used to contain the dice values
        * number is an int used to contain the player number
        * player_score is an int Array used to contain the score for the player. Each int corresponds to a row on the scoreboard.
        * assigned is a bool Array used to register whether a score item has been used by the player
        */

        public Dice[] hand = new Dice[5];
        int number;
        public int[] player_score = new int[15];
        public bool[] assigned = new bool[15];

        public Player(int num)
        /* The Player constructor takes an int as parameter, this int is used as the player number.
        *   The constructor then initiates the hand field by creating 5 instances of the class Dice
        * Then the assigned bool array is set to false and the player_score array is set to 0.
        */
        {
            this.number = num;
            for (int i = 0; i < 5; i++)
            {
                hand[i] = new Dice();
            }
            for (int i = 0; i < 15; i++)
            {
                assigned[i] = false;
                player_score[i] = 0;
            }
        }


        public bool Bonus()
        // The bonus method checks if the player has enough points to get the 50 point bonus.
        // Returns true if the 6 first items of player_score combined are 63 or greater, otherwise false.       
        {
            return (player_score[0] + player_score[1] + player_score[2] + player_score[3] + player_score[4] + player_score[5] >= 63);

        }

        public void Throw_dice(Random r)
        // The throw_dice method takes the randomizer r as parameter. It then calls the roll function of each Dice.
        {
            for (int i = 0; i < 5; i++)
            {
                hand[i].Roll(r);
            }
            Console.WriteLine();
            Console.WriteLine("Spelare {0} slog: {1}, {2}, {3}, {4}, {5}", number, hand[0].dice_score, hand[1].dice_score, hand[2].dice_score, hand[3].dice_score, hand[4].dice_score);

        }

        public void Hold_all(bool h)
        // The Hold method sets all Dices held field to the given bool parameter
        {
            for (int i = 0; i < 5; i++)
            {
                hand[i].held = h;
            }
        }

        public void Assign_score()
        // The assign_score method lists all available options for the player to use for the score of the current round
        {

            Console.WriteLine("Möjliga val:");
            Console.WriteLine();

            // This for loop runs through all unassigned rows and list the score by calling the calculate method for each row.

            for (int i = 0; i < 15; i++)
            {
                if (!assigned[i])
                {
                    int p = Calculate(i);
                    Console.WriteLine("{0}: {1} - {2} poäng", i + 1, ScoreBoard.name[i], p);
                }
            }

            // The user is prompted to select a row to assign this round's score to. No error handling is used here.
            Console.WriteLine();
            Console.Write("Välj rad: ");
            string input3 = Console.ReadLine();
            int item = int.Parse(input3) - 1;

            // assigned is set to true for the selected row, and the score for that row is added to player_score
            assigned[item] = true;
            player_score[item] = Calculate(item);
        }

        public int Calculate(int item)

        // The calculate method is used to calculate the score for the current throw for the row specified by the parameter int item
        {
            // counter is used to keep the score and is returned at the end of the method
            int counter = 0;

            // the int array res is used to keep the score. The values from hand are transferred to res.
            int[] res = new int[5];
            for (int i = 0; i < 5; i++)
            {
                res[i] = hand[i].dice_score;
            }

            // res is sorted in descending order to simplify scoring
            Array.Sort(res,
                delegate(int a, int b)
                {
                    return b - a; //Normal compare is a-b
                });

            // the actual score calculation is done in the switch statement below. 
            switch (item)
            {
                case 0: //ones
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 1)
                            counter++;
                    }
                    break;
                case 1: //twos
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 2)
                            counter = counter + 2;
                    }
                    break;
                case 2: //threes
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 3)
                            counter = counter + 3;
                    }
                    break;
                case 3: //fours
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 4)
                            counter = counter + 4;
                    }
                    break;
                case 4: //fives
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 5)
                            counter = counter + 5;
                    }
                    break;
                case 5: //sixes
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 6)
                            counter = counter + 6;
                    }
                    break;
                case 6: //Pair. If two consecutive dices are equal, their value is stored in counter and the loop ends.
                    for (int i = 0; i < 4; i++)
                    {
                        if (res[i] == res[i + 1])
                        {
                            counter = res[i] * 2;
                            break;
                        }
                    }
                    break;
                case 7: //Two pairs. If two consecutive dices are equal, their value is stored in first, count is set to one and one step of the for loop is skipped. 
                    // if a second pair is found, first * 2 and the current value of res * 2 is stored in counter
                    int count = 0;
                    int first = 0;
                    for (int i = 0; i < 4; i++)
                    {
                        if (res[i] == res[i + 1])
                        {
                            count++;
                            if (count == 1)
                            {
                                first = res[i];
                                i++;
                            }
                            else if (res[i] != first)
                            {
                                counter = first * 2 + res[i] * 2;
                            }
                        }

                    }
                    break;
                case 8: // Three of a kind. If three consecutive values are equal, 3 * that value is stored in counter
                    for (int i = 0; i < 3; i++)
                    {
                        if (res[i] == res[i + 1] && res[i + 1] == res[i + 2])
                            counter = res[i] * 3;
                    }
                    break;
                case 9: // Four of a kind. If four consecutive values are equal, 4 * that value is stored in counter
                    for (int i = 0; i < 2; i++)
                    {
                        if (res[i] == res[i + 1] && res[i + 1] == res[i + 2] && res[i + 2] == res[i + 3])
                            counter = res[i] * 4;
                    }
                    break;
                case 10: // Small straight. As soon as a dice is not correct in sequence, counter is set to 0 and the loop is ended. If all dices are correct, 15 is stored in counter
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] != 5 - i)
                        {
                            counter = 0;
                            break;
                        }
                        counter = 15;
                    }
                    break;
                case 11: // Large straight. As soon as a dice is not correct in sequence, counter is set to 0 and the loop is ended. If all dices are correct, 20 is stored in counter
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] != 6 - i)
                        {
                            counter = 0;
                            break;
                        }
                        counter = 20;
                    }
                    break;
                case 12: // Full house. If the first two values are equal and the last three are equal, their total score is stored in counter. The same applies if the first three are equal and the last two are equal.
                    if (res[0] == res[1] && res[1] == res[2])
                    {
                        if (res[3] == res[4] && res[4] != res[0])
                            counter = 3 * res[0] + 2 * res[4];
                    }
                    else if (res[2] == res[3] && res[3] == res[4])
                    {
                        if (res[0] == res[1] && res[4] != res[0])
                            counter = 3 * res[4] + 2 * res[0];
                    }
                    break;
                case 13: // Chance. The sum of the 5 values is stored in counter
                    counter = res[0] + res[1] + res[2] + res[3] + res[4];
                    break;
                case 14: // Yatzee. If all five values are equal, their total value + 50 is stored in counter.
                    if (res[0] == res[1] && res[1] == res[2] && res[2] == res[3] && res[3] == res[4])
                        counter = 50 + res[0] * 5;
                    break;
                default:
                    break;
            }
            return counter;
        }

        public int Total_score()
        // The method total_score calculates the total score by adding all the elements in the player_score array and adding 50 if bonus() returns true
        {

            int tot = player_score.Sum();
            if (Bonus())
                tot += 50;
            return tot;
        }
    }
}