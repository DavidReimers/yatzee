﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Yatzee
{
    /// <summary>
    /// Main class for Yatzee game, extends class System.Windows.Forms.Form
    /// </summary>
    public partial class Yatzy : Form
    {

        private int     active_player;
        private int     numberofplayers;
        private int     round;
        private Random  r;
        
        private Label[]     name_field = new Label[18];
        private Label[,]    player_field = new Label[4, 18];
        private Label[]     player_label = new Label[4];
        private Dice[]      dices = new Dice[5];
        private Player[]    players = new Player[4];

        private TableLayoutPanel[] player_panel = new TableLayoutPanel[4];

        public static Color[] player_colors = { Color.Aquamarine, Color.BlanchedAlmond, Color.DarkGoldenrod, Color.Crimson };
        public static string[] name = { "Ettor", "Tvåor", "Treor", "Fyror", "Femmor", "Sexor", "Summa", "Bonus", "Par", "Två par", "Triss", "Fyrtal", "Liten stege", "Stor stege", "Kåk", "Chans", "Yatzy", "Total" };
        
        public Yatzy()
        {
            
            InitializeComponent();

            r = new Random();

            player_panel[0] = playerPanel0;
            player_panel[1] = playerPanel1;
            player_panel[2] = playerPanel2;
            player_panel[3] = playerPanel3;

            for (int i = 0; i < 5; i++ )
            {
                dices[i] = new Dice();               
                dices[i].Location = new Point(327, 52 + i * 69);
                dices[i].Name = "dice" + (i + 1);
                Controls.Add(dices[i]);
                dices[i].Click += new EventHandler(dice_Click);
            }

            for (int i = 0; i < 18; i++)
            {
                name_field[i] = new Label();
                name_field[i].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
                name_field[i].Text = name[i];
                name_field[i].TextAlign = ContentAlignment.MiddleLeft;
                name_field[i].Margin = Padding.Empty;
                namePanel.Controls.Add(name_field[i]);
                name_field[i].Cursor = Cursors.Hand;
                name_field[i].Click += new EventHandler(namefield_Click);
                name_field[i].Enabled = false;
            } 

            for (int i = 0; i < 4; i++)
            {
                player_label[i] = new Label();
                player_label[i].BorderStyle = BorderStyle.None;
                player_label[i].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
                player_label[i].ForeColor = Color.Black;
                player_label[i].Location = new Point(126 + 40 * i, 20);
                player_label[i].Name = "label" + (i + 2) + "_0";
                player_label[i].Size = new Size(40, 22);
                player_label[i].Text = (i + 1).ToString();
                player_label[i].TextAlign = ContentAlignment.MiddleCenter;
                player_label[i].BackColor = player_colors[i];
                Controls.Add(player_label[i]);

                for (int j = 0; j < 18; j++)
                {
                    player_field[i, j] = new Label();
                    player_field[i, j].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                    player_field[i, j].Name = "label" + i + "_" + j;
                    player_field[i, j].TextAlign = ContentAlignment.MiddleCenter;
                    player_field[i, j].Margin = Padding.Empty;
                    player_field[i, j].Cursor = Cursors.Hand;
                    
                    player_panel[i].Controls.Add(player_field[i, j],0,j);
                  
                    player_field[i, j].Anchor = AnchorStyles.Left;
                    player_field[i, j].Click += new EventHandler(playerfield_Click);
                }
            }

            //Then an array of Player is created, with a new instance for each player

            for (int i = 0; i < 4; i++)
            {
                players[i] = new Player();
            }

            NewGame();
                
        }

        /// <summary>
        /// Starts a new game. Graphic components are initialized and the number of players is set.
        /// </summary>
        private void NewGame()
        {
            foreach (Label l in player_field)
            {
                l.Enabled = false;
                l.Text = "";
            }

            foreach (Label l in player_label)
            {
                l.Visible = false;
            }

            foreach (TableLayoutPanel tlp in player_panel)
            {
                tlp.Visible = false;
            }    

            foreach (Label l in name_field)
            {
                l.Enabled = false;
            } 
       
            numberofplayers = GetPlayers();

            for (int i = 0; i < numberofplayers; i++)
            {
                player_panel[i].Visible = true;
                player_label[i].Visible = true;
                Hold_all();
                players[i].Unassign();
            }

            // We set the starting player
            active_player = 0;
            activeplayerlabel.Text = "Spelare " + (active_player + 1);
            activeplayerlabel.BackColor = player_colors[active_player];
                     
            for (int i = 0; i < 5; i++)
            {
                dices[i].Enabled = true;
                dices[i].BorderStyle = BorderStyle.None;
            }
            
            throwbutton.Enabled = true;

            round = 1;

            Next_throw();
        }

        /// <summary>
        /// Asks the user to enter the number of players (1-4). If user presses Cancel, the application is ended.
        /// </summary>
        /// <returns>int between 1 and 4</returns>
        private int GetPlayers()
        {
            // The method getPlayers asks the user to enter the number of players. 
            // Basic error handling is being made in this method.

            int number;
            bool input_error = true;

            // The do loop runs until a valid integer between 1 and 4 is entered as a response.
            // The input is checked by the TryParse method. If the TryParse returns false, the following checks (>0 and <5) 
            // are not performed
            do
            {
                string myValue = Interaction.InputBox("Ange antal spelare (1-4): ", "Antal spelare", "2",200,240);

                if (myValue == "")
                {
                    this.Close();
                    return 0;
                }

                if (int.TryParse(myValue, out number) && number > 0 && number < 5)
                {
                    input_error = false;
                }
                else
                {
                    MessageBox.Show(this, "Du måste ange ett tal mellan 1 och 4. Försök igen","Fel", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } while (input_error);

            return number;
        }

        /// <summary>
        /// Rolls all dice and displays all possible scores for the updated dice set.
        /// </summary>
        private void Next_throw()
        {
            for (int i = 0; i < 5; i++)
            {
                dices[i].Roll(r);
            }

            if (round == 3)
            {
                throwbutton.Enabled = false;

                for (int i = 0; i < 5; i++)
                {
                    dices[i].Enabled = false;
                }
            }

            for (int i = 0; i < 17; i++)
            {
                if (!players[active_player].assigned[i])
                {
                    int p = Calculate(i);

                    player_field[active_player, i].BackColor = Color.FloralWhite;
                    name_field[i].BackColor = Color.FloralWhite;
                    
                    if (p > 0)
                    {
                        player_field[active_player, i].BackColor = Color.LightGreen;
                        name_field[i].BackColor = Color.LightGreen;
                    }
                    
                    name_field[i].Enabled = true;
                    player_field[active_player, i].Text = p.ToString();
                    player_field[active_player,i].Enabled = true;
                }
            }            
        }

        /// <summary>
        /// Calculates and displays the winner, and then asks the user if they want to play again.
        /// </summary>
        /// <returns>true if user responds Yes, false otherwise</returns>
        private bool Play_again()
        {
             // When the board is full, the game calculates the winner
            int winner = 0;
            int best = 0;

            for (int i = 0; i < numberofplayers; i++)
            {
           
                if (Total(i) > best)
                {
                    best = Total(i);
                    winner = i + 1;
                }                
            }

            MessageBox.Show("Spelare " + winner + " har vunnit!", "Spelet är slut!");
          
            // The user is asked if they want to play again. 

            DialogResult input4 = MessageBox.Show("Vill du spela igen (Ja/Nej)? ", "Spela igen", MessageBoxButtons.YesNo);
            return (input4 == DialogResult.Yes);
            
        }

        /// <summary>
        /// Calculates the total score for the first 6 rows for the active player
        /// </summary>
        /// <returns>sum of row 1-6 for the active player</returns>
        private int Sum()
        {
            int numbersum = 0;
            int n;
            for (int i = 0; i < 6; i++)
            {
                if (int.TryParse(player_field[active_player, i].Text, out n))
                    numbersum += n;
            }
            return numbersum;
        }

        /// <summary>
        /// Calculates total score for player p.
        /// </summary>
        /// <param name="p">int that represents the player number</param>
        /// <returns>total score for the player</returns>
        private int Total(int p)
        {
            int total = 0;
            int n;
            for (int i = 0; i < 17; i++)
            {
                if (i != 6 && int.TryParse(player_field[p, i].Text, out n))
                    total += n;
            }

            return total;
        }

        /// <summary>
        /// The Hold method sets all Dices held field to false.
        /// </summary>
        private void Hold_all()

        {
            for (int i = 0; i < 5; i++)
            {
                dices[i].held = false;
            }
        }

        /// <summary>
        /// Calculate the score for the current throw for the row specified by the parameter int item.
        /// </summary>
        /// <param name="item">Row number to calculate score for</param>
        /// <returns>The current score for the given row</returns>
        private int Calculate(int item)

        // The calculate method is used to calculate the score for the current throw for the row specified by the parameter int item
        {
            // counter is used to keep the score and is returned at the end of the method
            int counter = 0;

            // the int array res is used to keep the score. The values from hand are transferred to res.
            int[] res = new int[5];
            for (int i = 0; i < 5; i++)
            {
                res[i] = dices[i].dice_score;
            }

            // res is sorted in descending order to simplify scoring
            Array.Sort(res,
                delegate(int a, int b)
                {
                    return b - a; //Normal compare is a-b
                });

            // the actual score calculation is done in the switch statement below. 
            switch (item)
            {
                case 0: //ones
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 1)
                            counter++;
                    }
                    break;
                case 1: //twos
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 2)
                            counter = counter + 2;
                    }
                    break;
                case 2: //threes
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 3)
                            counter = counter + 3;
                    }
                    break;
                case 3: //fours
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 4)
                            counter = counter + 4;
                    }
                    break;
                case 4: //fives
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 5)
                            counter = counter + 5;
                    }
                    break;
                case 5: //sixes
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] == 6)
                            counter = counter + 6;
                    }
                    break;
                case 8: //Pair. If two consecutive dices are equal, their value is stored in counter and the loop ends.
                    for (int i = 0; i < 4; i++)
                    {
                        if (res[i] == res[i + 1])
                        {
                            counter = res[i] * 2;
                            break;
                        }
                    }
                    break;
                case 9: //Two pairs. If two consecutive dices are equal, their value is stored in first, count is set to one and one step of the for loop is skipped. 
                    // if a second pair is found, first * 2 and the current value of res * 2 is stored in counter
                    int count = 0;
                    int first = 0;
                    for (int i = 0; i < 4; i++)
                    {
                        if (res[i] == res[i + 1])
                        {
                            count++;
                            if (count == 1)
                            {
                                first = res[i];
                                i++;
                            }
                            else if (res[i] != first)
                            {
                                counter = first * 2 + res[i] * 2;
                            }
                        }

                    }
                    break;
                case 10: // Three of a kind. If three consecutive values are equal, 3 * that value is stored in counter
                    for (int i = 0; i < 3; i++)
                    {
                        if (res[i] == res[i + 1] && res[i + 1] == res[i + 2])
                            counter = res[i] * 3;
                    }
                    break;
                case 11: // Four of a kind. If four consecutive values are equal, 4 * that value is stored in counter
                    for (int i = 0; i < 2; i++)
                    {
                        if (res[i] == res[i + 1] && res[i + 1] == res[i + 2] && res[i + 2] == res[i + 3])
                            counter = res[i] * 4;
                    }
                    break;
                case 12: // Small straight. As soon as a dice is not correct in sequence, counter is set to 0 and the loop is ended. If all dices are correct, 15 is stored in counter
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] != 5 - i)
                        {
                            counter = 0;
                            break;
                        }
                        counter = 15;
                    }
                    break;
                case 13: // Large straight. As soon as a dice is not correct in sequence, counter is set to 0 and the loop is ended. If all dices are correct, 20 is stored in counter
                    for (int i = 0; i < 5; i++)
                    {
                        if (res[i] != 6 - i)
                        {
                            counter = 0;
                            break;
                        }
                        counter = 20;
                    }
                    break;
                case 14: // Full house. If the first two values are equal and the last three are equal, their total score is stored in counter. The same applies if the first three are equal and the last two are equal.
                    if (res[0] == res[1] && res[1] == res[2])
                    {
                        if (res[3] == res[4] && res[4] != res[0])
                            counter = 3 * res[0] + 2 * res[4];
                    }
                    else if (res[2] == res[3] && res[3] == res[4])
                    {
                        if (res[0] == res[1] && res[4] != res[0])
                            counter = 3 * res[4] + 2 * res[0];
                    }
                    break;
                case 15: // Chance. The sum of the 5 values is stored in counter
                    counter = res[0] + res[1] + res[2] + res[3] + res[4];
                    break;
                case 16: // Yatzee. If all five values are equal, their total value + 50 is stored in counter.
                    if (res[0] == res[1] && res[1] == res[2] && res[2] == res[3] && res[3] == res[4])
                        counter = 50 + res[0] * 5;
                    break;
                default:
                    break;
            }
            return counter;
        }

        private void Score(int row)
        {
            // assigned is set to true for the selected row, and the score for that row is added to player_score
            players[active_player].assigned[row] = true;
            for (int i = 0; i < 18; i++)
            {
                if (!players[active_player].assigned[i])
                    player_field[active_player, i].Text = "";
            }
            
            player_field[active_player, 6].Text = Sum().ToString();

            if (Sum() >= 63)
                player_field[active_player, 7].Text = "50";
            else
                player_field[active_player, 7].Text = "0";

            player_field[active_player, 17].Text = Total(active_player).ToString();

            for (int i = 0; i < 18; i++)
            {
                player_field[active_player, i].BackColor = Color.FromArgb(224, 224, 224);
                player_field[active_player, i].Enabled = false;

                name_field[i].BackColor = Color.FromArgb(224, 224, 224);
                name_field[i].Enabled = false;     
            }

            bool full = true;
            for (int i = 0; i < numberofplayers && full; i++)
            {
                for (int j = 0; j < 18 && full; j++)
                {
                    if (!players[i].assigned[j])
                        full = false;
                }
            }

            if (full)
            {
                if (Play_again())
                    NewGame();
                else
                    this.Close(); 
            }
            else
            {
                active_player++;
                if (active_player == numberofplayers)
                    active_player = 0;

                round = 1;
                activeplayerlabel.Text = "Spelare " + (active_player + 1);
                activeplayerlabel.BackColor = player_colors[active_player];

                throwbutton.Enabled = true;

                Hold_all();
                for (int i = 0; i < 5; i++)
                {
                    dices[i].Enabled = true;
                    dices[i].BorderStyle = BorderStyle.None;
                }
                Next_throw();
            }
        }


        private void playerfield_Click(object sender, EventArgs e)
        {
            Label clicked = (Label)sender;

            int num = int.Parse(clicked.Name.Substring(clicked.Name.IndexOf('_') + 1));

            Score(num);
        }

        private void namefield_Click(object sender, EventArgs e)
        {
            Label clicked = (Label) sender;
            int num = Array.IndexOf(name, clicked.Text);

            Score(num);
        } 

        private void dice_Click(object sender, EventArgs e)
        {
            Dice clicked = (Dice) sender;
            int num = Array.IndexOf(dices, (Dice) sender);

            if (clicked.BorderStyle == BorderStyle.None)
            {
                clicked.BorderStyle = BorderStyle.FixedSingle;
                clicked.held = true;
            }
            else
            {
                clicked.BorderStyle = BorderStyle.None;
                clicked.held = false;
            }
        }

        private void throwbutton_Click(object sender, EventArgs e)
        {
            round++;
            Next_throw();
        }

    }
}
