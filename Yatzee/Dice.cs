﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;


namespace Yatzee
{
    public class Dice : Label
    {
        private int _dice_score;
        public bool held;
        private static Image[] dice_images = {Image.FromFile("C:\\Users\\David\\documents\\visual studio 2013\\Projects\\Yatzee\\Yatzee\\dice-1.png"),
                                                Image.FromFile("C:\\Users\\David\\documents\\visual studio 2013\\Projects\\Yatzee\\Yatzee\\dice-2.png"),
                                                Image.FromFile("C:\\Users\\David\\documents\\visual studio 2013\\Projects\\Yatzee\\Yatzee\\dice-3.png"),
                                                Image.FromFile("C:\\Users\\David\\documents\\visual studio 2013\\Projects\\Yatzee\\Yatzee\\dice-4.png"),
                                                Image.FromFile("C:\\Users\\David\\documents\\visual studio 2013\\Projects\\Yatzee\\Yatzee\\dice-5.png"),
                                                Image.FromFile("C:\\Users\\David\\documents\\visual studio 2013\\Projects\\Yatzee\\Yatzee\\dice-6.png")};

        private int _borderWidth = 2;

        public Dice()
        {
            Size = new Size(66, 66);
            BackColor = Color.FromArgb(192, 255, 192);
            BorderStyle = BorderStyle.None;
            ImageAlign = ContentAlignment.MiddleCenter;
            Cursor = Cursors.Hand;
        }
        
        public int dice_score
        {
            get { return _dice_score; }
        }
        
        public int BorderWidth
        {
            get { return _borderWidth; }
            set
            {
                _borderWidth = value;
                Invalidate();
            }
        }

        private Color _borderColor = Color.Black;
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            int xy = 0;
            int width = this.ClientSize.Width;
            int height = this.ClientSize.Height;
            Pen pen = new Pen(_borderColor);
            if (this.BorderStyle == BorderStyle.FixedSingle)
            {
                for (int i = 0; i < _borderWidth; i++)
                    e.Graphics.DrawRectangle(pen, xy + i, xy + i, width - (i << 1) - 1, height - (i << 1) - 1);
            }
        }

        public void Roll(Random r)
        // The method roll takes the random object as parameter and returns a new random int between 1 and 6 to use as the score for that dice.
        {
            if (!held)
            {
                _dice_score = r.Next(1, 7);
                Image = dice_images[dice_score - 1];
                
            }
        }
    }
}
